int linearSearch(auto Data, auto key)
{
	for (int i = 0; i < Data.size();i++)
	{
		if (Data[i] == key)
		{
			return 1; //this is done once the key has been found.
		}
	}
	return -1; // the key doesn't exist in the data set.
}
